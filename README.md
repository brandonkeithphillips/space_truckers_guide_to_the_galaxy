# Space Trucker's Guide to the Galaxy
### It works!<sup>*</sup>

Welcome to the unofficial official guide to most things Elite: Dangerous!

This application was made initially as a tertiary project, and I've felt the need to continue development on this application. This application is non-commercial and open source!

This is a *fanmade application* and is in no way affiliated or endorsed by Frontier Developments. Frontier Developments was not involved in the making of this application.

-----
### TODO
Oof, lotta stuff to work on... but I guess it's stuff to look forward to!

| Features | Status |
| --- | :---:|
| Activity that lists categories of ships | TODO (Priority) |
| Activity that lists in-game ships | TODO (Priority) |
| Activity that details selected ship from the listing activity | TODO (Priority) |
| Commodity activity using JSON commodity data from EDDB | TODO (bonus feature) |

-----
### References
Some assets were used from [CMDR SpyTech's Elite: Dangerous Assets](https://edassets.org/).

Player sourced commodity market data taken from [EDDB](https://eddb.io/api).

Universe is from [Frontier Developments' *Elite: Dangerous*](https://www.frontier.co.uk/).
