package tech.guac.spacetruckersguidetothegalaxy.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tech.guac.spacetruckersguidetothegalaxy.R
import tech.guac.spacetruckersguidetothegalaxy.model.Ship

/**
 * RecyclerView adapter for the Ship class.
 * Implemented by
 */
class ShipAdapter(val context: Context, val shipList: ArrayList<Ship>, val layout: Int) : RecyclerView.Adapter<ShipViewHolder>() {

    // Creates the View and the ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShipViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        return ShipViewHolder(inflater.inflate(layout, parent, false))
    }

    // gets item count of list, presumably to create a set number of views to create.
    override fun getItemCount(): Int {
        return shipList.count()
    }

    override fun onBindViewHolder(holder: ShipViewHolder, position: Int) {
        val ship: Ship = shipList[position]
    }
}

class ShipViewHolder(layout: View) : RecyclerView.ViewHolder(layout) {
    val manufacturerName: TextView = layout.findViewById(R.id.manufacturer_name)
    val manufacturerShipCount: TextView = layout.findViewById(R.id.manufacturer_shipcount)
    val manufacturerImage: ImageView = layout.findViewById(R.id.manufacturer_image)

}