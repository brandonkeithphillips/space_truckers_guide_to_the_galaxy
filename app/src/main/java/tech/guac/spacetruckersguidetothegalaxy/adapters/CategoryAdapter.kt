package tech.guac.spacetruckersguidetothegalaxy.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import tech.guac.spacetruckersguidetothegalaxy.R
import tech.guac.spacetruckersguidetothegalaxy.model.Category

class CategoryAdapter(val context: Context, private val categoryList: List<Category>, val layout: Int) : RecyclerView.Adapter<CategoryViewHolder>() {

    // creates the View and creates the ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val inflater: LayoutInflater = LayoutInflater.from(context)
        return CategoryViewHolder(inflater.inflate(layout, parent, false))
    }

    // gets category count by category type.
    override fun getItemCount(): Int {
        return categoryList.count()
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categoryList[position]
        holder.categoryTitle.text = category.categoryName
        holder.categoryImage.setImageResource(
                context.resources.getIdentifier(category.imageResourceName, "drawable", context.packageName)
        )
    }
}

class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val categoryImage: ImageView = itemView.findViewById(R.id.category_image)
    val categoryTitle: TextView = itemView.findViewById(R.id.category_title)
}